﻿namespace Randomize.Models
{
    public class ItemsModel
    {
        public string ItemName { get; set; }
        public string Hsn { get; set; }
        public decimal SalePrice { get; set; }
        public decimal GST { get; set; }
        public string Per { get; set; }
    }
}
