﻿using System;
using System.IO;
using System.Collections.Generic;
using Randomize.Models;
using Newtonsoft.Json;
using System.Configuration;
using A4BillLibrary.BillModel.ItemsModel;

namespace Randomize
{
    public class Randomize
    {
        private const int _maxQty = 32;
        private const int _maximumLinesPerPage = 20;

        private List<ItemsModel> _itemsList;
        private int _maxLineItems;
        private int _currentLineItems;
        private int _currentQty;
        private Random _wizardOfOz;
        private decimal _amountValue;
        private decimal _delimitedAmount;
        private int _serialNumber;
        private string _jsonPath;

        public Randomize(int filledLines, decimal AmountValue, int serialNumber)
        {
            _jsonPath = string.Format("{0}AksGst.json", ConfigurationManager.AppSettings["jsonPath"]);
            loadItems(_jsonPath);
            _wizardOfOz = new Random();
            _maxLineItems = pickNumberOfLines(_maximumLinesPerPage - filledLines);
            _currentLineItems = 0;
            _amountValue = AmountValue;
            _serialNumber = serialNumber;
        }

        public List<BillingItemsManual> AutoGenItemListWithRetry(int retryCount)
        {
            while(retryCount != 0)
            {
                try
                {
                    loadItems(_jsonPath);
                    return autoGenItemList();
                }
                catch(NotSupportedException)
                {
                    retryCount--;
                }
            }
            throw new NotSupportedException("Retry Failed");
        }

        private List<BillingItemsManual> autoGenItemList()
        {
            var outPutList = new List<BillingItemsManual>();
            while (_amountValue != 0)
            {
                var selectedRandomItem = pickRandomItem();
                if (selectedRandomItem != null)
                    outPutList.Add(selectedRandomItem);
            }
            return outPutList;
        }

        private BillingItemsManual pickRandomItem()
        {
            _currentLineItems += 1;
            if (_currentLineItems <= _maxLineItems)
            {
                var pickedItem = pickItemFromList();
                _serialNumber++;
                return new BillingItemsManual
                {
                    Sno = _serialNumber.ToString(),
                    Qty = _currentQty.ToString(),
                    ItemName = pickedItem.ItemName,
                    GST = pickedItem.GST.ToString(),
                    Hsn = pickedItem.Hsn.ToString(),
                    SalePrice = pickedItem.SalePrice.ToString(),
                    Per = pickedItem.Per,
                    Discount = 0.ToString()
                };
            }
            else
            {
                if (_amountValue <= 10M)
                {
                    _amountValue = 0;
                    return null;
                }
                else if(_maxLineItems < 22)
                {
                    _maxLineItems++;
                    return null;
                }
                throw new NotSupportedException($"The left over amount ' {_amountValue} ' exceeded limits.");
            }
        }

        private ItemsModel pickItemFromList()
        {
            var iterationCounter = default(int);
            ItemsModel selectedObj;
            calculateDelimitedAmount();
            while (_amountValue != 0)
            {
                selectedObj = _itemsList[_wizardOfOz.Next(0, _itemsList.Count)];
                var price = selectedObj.SalePrice;
                var selectedQty = calculateQty(price);
                var deductedAmount = selectedQty * selectedObj.SalePrice;
                if (isEfficientSelection(price, selectedQty, deductedAmount))
                {
                    _amountValue = _amountValue - deductedAmount;
                    _itemsList.Remove(selectedObj);
                    return selectedObj;
                }

                iterationCounter++;
                if (iterationCounter >= 10 * _itemsList.Count)
                {
                    calculateDelimitedAmount();
                    iterationCounter = default(int);
                }
            }
            throw new NotSupportedException($"The amount ' {_amountValue} ' cannot be negative.");
        }

        private bool isEfficientSelection(decimal price, int quantity, decimal deductedAmount)
        {
            var isEfficient = false;
            if (price <= _delimitedAmount)
            {
                if (quantity <= _maxQty)
                {
                    if (deductedAmount <= _amountValue)
                    {
                        var efficiency = (deductedAmount / _delimitedAmount) * 100;
                        if (efficiency >= 92)
                        {
                            isEfficient = true;
                        }
                    }
                }
            }
            return isEfficient;
        }

        private void calculateDelimitedAmount()
        {
            var availableLines = _maxLineItems - _currentLineItems + 1;
            var tresholdAmount = _amountValue / availableLines;
            if (tresholdAmount < 250)
            {
                _delimitedAmount = tresholdAmount;
            }
            else
            {
                decimal variance = _wizardOfOz.Next(10, 98) / 100M;
                _delimitedAmount = (variance * tresholdAmount + tresholdAmount);
                if (_delimitedAmount > _amountValue)
                {
                    _delimitedAmount = tresholdAmount;
                }
            }
        }

        private int calculateQty(decimal salePrice)
        {
            var flooredQty = decimal.Floor(_delimitedAmount / salePrice);
            _currentQty = Convert.ToInt32(flooredQty);
            return _currentQty;
        }

        private int pickNumberOfLines(int lineItemBuffer)
        {
            int delimiter = Convert.ToInt32(lineItemBuffer * 0.25);
            return _wizardOfOz.Next(delimiter, lineItemBuffer);
        }

        private void loadItems(string path)
        {
            var jsonFile = File.ReadAllText(path);
            _itemsList = JsonConvert.DeserializeObject<List<ItemsModel>>(jsonFile);
        }
    }
}
