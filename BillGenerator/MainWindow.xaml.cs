﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;
using A4BillLibrary;
using A4BillLibrary.BillModel;
using A4BillLibrary.BillModel.HeaderModel;
using A4BillLibrary.BillModel.ItemsModel;

namespace BillGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<BillingItemsManual> BillingList;
        AutoFill.AutoFill autoFillCustomerDetails;
        List<string> perList = new List<string> { "pcs", "lgh", "ft", "mt", "box", "doz", "bag", "hlfbg", "kg", "g", "load", "unit", "auto", "cft", "bond", "pkt", "roll", "bndl" };
        List<string> saleType = new List<string> { "credit", "cash" };
        int Index;
        string LButtonMode;
        string RButtonMode;
        DispatcherTimer Timer;
        int FlasherCount = 0;
        Control FlasherControl;
        string currentSno;
        bool isItemAdded;
        bool isAutoAddItems;
        public MainWindow()
        {
            InitializeComponent();
            InitializeImageComponents();
            Timer = new DispatcherTimer();
            BillingList = new List<BillingItemsManual>();
            invoiceNo.Text = A4Bill.invoiceNumber;
            billDate.Text = A4Bill.dateStamp;
            currentSno = A4Bill.sNo();
            autoFillCustomerDetails = new AutoFill.AutoFill();
            snoTxtBox.Text = currentSno;
            toggleButtonControls(true);
            Timer.Tick += Timer_Tick;
            Timer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            itemList.ItemsSource = BillingList;
            perComboBox.ItemsSource = perList;
            perComboBox.SelectedItem = perComboBox.Items[0];
            paymentMode.ItemsSource = saleType;
            paymentMode.SelectedItem = paymentMode.Items[0];
        }

        private void InitializeImageComponents()
        {
            updateIsCustomerNameAvailableImg(false);
        }

        private void updateIsCustomerNameAvailableImg(bool isValid)
        {
            Func<Storyboard> animate = () => {
                var story = new Storyboard();
                var fadeOut = new DoubleAnimation(1.0, 0.0, TimeSpan.FromSeconds(3.25));
                var fadeIn = new DoubleAnimation(0.0, 1.0, TimeSpan.FromSeconds(1.75));
                story.Children.Add(fadeOut);
                story.Children.Add(fadeIn);
                Storyboard.SetTargetName(fadeOut, isCustomerNameAvailableImg.Name);
                Storyboard.SetTargetName(fadeIn, isCustomerNameAvailableImg.Name);
                Storyboard.SetTargetProperty(fadeOut, new PropertyPath(OpacityProperty));
                Storyboard.SetTargetProperty(fadeIn, new PropertyPath(OpacityProperty));
                return story;
            };
            string uri;
            if (isValid)
            {
                uri = "Resources/Tick.png";
                isCustomerNameAvailableImg.IsEnabled = true;
            }
            else
            {
                uri = "Resources/X.png";
                isCustomerNameAvailableImg.IsEnabled = false;
            }
            animate().Begin(isCustomerNameAvailableImg);
            isCustomerNameAvailableImg.Source = new BitmapImage(new Uri(uri, UriKind.Relative));
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            FlasherCount += 1;
            if (FlasherCount % 2 != 0)
            {
                FlasherControl.BorderBrush = Brushes.OrangeRed;
            }
            else
            {
                FlasherControl.BorderBrush = Brushes.Transparent;
            }
            if (FlasherCount == 10)
            {
                Timer.Stop();
                FlasherCount = 0;
            }
        }

        private void itemList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedObject = (BillingItemsManual)itemList.SelectedItem;
            Index = BillingList.FindIndex(x => x.Sno == selectedObject.Sno);
            itemList.Visibility = Visibility.Collapsed;
            TopBar.Visibility = Visibility.Visible;
            ItemListAddControl.Visibility = Visibility.Visible;
            snoTxtBox.Text = selectedObject.Sno;
            salePriceTxtBox.Text = selectedObject.SalePrice;
            qtyTxtBox.Text = selectedObject.Qty;
            itemNameTxtBox.Text = selectedObject.ItemName;
            perComboBox.SelectedItem = selectedObject.Per;
            hsnTxtBox.Text = selectedObject.Hsn;
            gstTxtBox.Text = selectedObject.GST;
            discountTxtBox.Text = selectedObject.Discount;
            toggleButtonControls(false, false, true);
        }

        private void LBtn_Click(object sender, RoutedEventArgs e)
        {
            if (LButtonMode == "editItem")
            {
                itemList.Visibility = Visibility.Visible;
                TopBar.Visibility = Visibility.Collapsed;
                ItemListAddControl.Visibility = Visibility.Collapsed;
                toggleButtonControls(false, true);
                return;
            }
            if (LButtonMode == "editCancel")
            {
                TopBar.Visibility = Visibility.Visible;
                itemList.Visibility = Visibility.Collapsed;
                ItemListAddControl.Visibility = Visibility.Visible;
                toggleButtonControls(true);
                clearTextBox();
                return;
            }
        }

        private void RBtn_Click(object sender, RoutedEventArgs e)
        {
            if (RButtonMode == "print")
            {
                if (string.IsNullOrEmpty(itemNameTxtBox.Text) && string.IsNullOrEmpty(hsnTxtBox.Text) && string.IsNullOrEmpty(qtyTxtBox.Text) && string.IsNullOrEmpty(salePriceTxtBox.Text))
                {
                    if (string.IsNullOrEmpty(references.Text))
                    {
                        references.Text = " ";
                    }
                    if (string.IsNullOrEmpty(addressL2.Text))
                    {
                        addressL2.Text = " ";
                    }
                    if (string.IsNullOrEmpty(addressL3.Text))
                    {
                        addressL2.Text = " ";
                    }
                    if (string.IsNullOrEmpty(customerName.Text))
                    {
                        customerName.Text = " ";
                    }
                    if (string.IsNullOrEmpty(customerMobile.Text))
                    {
                        customerMobile.Text = " ";
                    }
                    if (string.IsNullOrEmpty(addressL1.Text))
                    {
                        addressL1.Text = " ";
                    }
                    A4Bill.validateInvoiceNumber(invoiceNo.Text);
                    if(isAutoAddItems)
                    {
                        var billingListCount = BillingList.Count;
                        var randomize = new Randomize.Randomize(billingListCount, Convert.ToDecimal(autoFillAmount.Text), billingListCount);
                        try
                        {
                            var autoGenList = randomize.AutoGenItemListWithRetry(7);
                            foreach (var item in autoGenList)
                            {
                                BillingList.Add(item);
                            }
                        }
                        catch (NotSupportedException)
                        {
                            autoFillAmount.Text = "Enter the Amount";
                            autoFillAmount.BorderBrush = Brushes.Red;
                            return;
                        }
                    }
                    var printObj = new Bill
                    {
                        BillHeader = new Header
                        {
                            billDetails = new billDetails
                            {
                                billDate = billDate.Text,
                                billReferences = references.Text,
                                deliveredTo = deliveredTo.Text,
                                invoiceNo = invoiceNo.Text,
                                paymentMode = paymentMode.Text
                            },
                            clientDetails = new clientDetails
                            {
                                addressL1 = addressL1.Text,
                                addressL2 = addressL2.Text,
                                addressL3 = addressL3.Text,
                                customerGSTIN = customerGSTIN.Text,
                                customerMobile = customerMobile.Text,
                                customerName = customerName.Text
                            }
                        },
                        BilledItems = BillingList
                    };
                    autoFillCustomerDetails.ModifyRecords(customerName.Text, new AutoFill.Models.CustomerDetails
                    {
                        MobileNumber = customerMobile.Text,
                        GSTIN = customerGSTIN.Text,
                        AddressLine1 = addressL1.Text,
                        AddressLine2 = addressL2.Text,
                        AddressLine3 = addressL3.Text
                    });
                    A4Bill.generateBill(printObj);
                    this.Close();
                    return;
                }
                else
                {
                    isItemAdded = false;
                    itemAddBtn.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
                    if (isItemAdded)
                        RBtn.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
                }
            }
            if (RButtonMode == "editOk")
            {
                BillingList.RemoveAt(Index);
                BillingList.Insert(Index, new BillingItemsManual
                {
                    Discount = discountTxtBox.Text,
                    GST = gstTxtBox.Text,
                    Hsn = hsnTxtBox.Text,
                    Per = perComboBox.SelectionBoxItem.ToString(),
                    ItemName = itemNameTxtBox.Text,
                    Qty = qtyTxtBox.Text,
                    SalePrice = salePriceTxtBox.Text,
                    Sno = snoTxtBox.Text
                });
                itemList.ItemsSource = BillingList;
                itemList.Items.Refresh();
                TopBar.Visibility = Visibility.Visible;
                ItemListAddControl.Visibility = Visibility.Visible;
                toggleButtonControls(true);
                clearTextBox();
                return;
            }
            if (RButtonMode == "closeBillList")
            {
                itemList.Visibility = Visibility.Collapsed;
                TopBar.Visibility = Visibility.Visible;
                ItemListAddControl.Visibility = Visibility.Visible;
                toggleButtonControls(true);
                return;
            }
        }
        void clearTextBox()
        {
            snoTxtBox.Text = currentSno;
            salePriceTxtBox.Text = string.Empty;
            qtyTxtBox.Text = string.Empty;
            itemNameTxtBox.Text = string.Empty;
            perComboBox.SelectedItem = string.Empty;
            hsnTxtBox.Text = string.Empty;
            gstTxtBox.Text = string.Empty;
            discountTxtBox.Text = "0";
        }

        private void itemAddBtn_Click(object sender, RoutedEventArgs e)
        {
            bool validEntry = isValidConrol(itemNameTxtBox) && isValidConrol(hsnTxtBox) && isValidConrol(qtyTxtBox) && isValidConrol(salePriceTxtBox) && isValidConrol(discountTxtBox) && isValidConrol(gstTxtBox);
            if (validEntry)
            {
                isItemAdded = true;
                var newItem = new BillingItemsManual
                {
                    Discount = discountTxtBox.Text,
                    GST = gstTxtBox.Text,
                    Hsn = hsnTxtBox.Text,
                    Per = perComboBox.SelectionBoxItem.ToString(),
                    ItemName = itemNameTxtBox.Text,
                    Qty = qtyTxtBox.Text,
                    SalePrice = salePriceTxtBox.Text,
                    Sno = snoTxtBox.Text
                };
                BillingList.Add(newItem);
                currentSno = A4Bill.sNo();
                clearTextBox();
                itemNameTxtBox.Focus();
                if(BillingList.Count==22)
                {
                    itemNameTxtBox.IsReadOnly = true;
                    qtyTxtBox.IsReadOnly = true;
                    salePriceTxtBox.IsReadOnly = true;
                    perComboBox.IsReadOnly = true;
                    gstTxtBox.IsReadOnly = true;
                    discountTxtBox.IsReadOnly = true;
                    hsnTxtBox.IsReadOnly = true;
                }
            }
        }
        private void toggleButtonControls(bool isBillScreen, bool isItemListScreen = false, bool isItemEditScreen = false)
        {
            if (isBillScreen)
            {
                LBtn.Visibility = Visibility.Visible;
                RBtn.Visibility = Visibility.Visible;
                itemAddBtn.Visibility = Visibility.Visible;
                LBtnTxt.Text = "Edit Items";
                RBtnTxt.Text = "Print";
                LButtonMode = "editItem";
                RButtonMode = "print";
            }
            if (isItemListScreen)
            {
                LBtn.Visibility = Visibility.Collapsed;
                RBtn.Visibility = Visibility.Visible;
                RBtnTxt.Text = "Back";
                RButtonMode = "closeBillList";
            }
            if (isItemEditScreen)
            {
                LBtn.Visibility = Visibility.Visible;
                RBtn.Visibility = Visibility.Visible;
                itemAddBtn.Visibility = Visibility.Collapsed;
                LBtnTxt.Text = "Cancel";
                RBtnTxt.Text = "Ok";
                LButtonMode = "editCancel";
                RButtonMode = "editOk";
            }
        }
        private bool isValidConrol(TextBox controlName)
        {
            bool isValid = string.IsNullOrEmpty(controlName.Text) ? false : true;
            if (!isValid)
            {
                FlasherControl = controlName;
                Timer.Start();
            }
            return isValid;
        }

        private void autoFillAmount_LostFocus(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(autoFillAmount.Text))
            {
                autoFillAmount.Text = "Enter the Amount";
            }
            decimal autoFillAmountValue;
            bool isValid = decimal.TryParse(autoFillAmount.Text, out autoFillAmountValue) ? true : false;
            if (!isValid)
            {
                FlasherControl = autoFillAmount;
                Timer.Start();
            }
        }

        private void autoFillAmount_GotFocus(object sender, RoutedEventArgs e)
        {
            if (autoFillAmount.Text== "Enter the Amount")
            {
                autoFillAmount.Text = string.Empty;
            }
        }

        private void isAutofill_Checked(object sender, RoutedEventArgs e)
        {
            autoFillAmount.Visibility = Visibility.Visible;
            isAutoAddItems = true;
        }

        private void isAutofill_Unchecked(object sender, RoutedEventArgs e)
        {
            autoFillAmount.Visibility = Visibility.Collapsed;
            isAutoAddItems = false;
        }

        private void customerName_KeyUp(object sender, KeyEventArgs e)
        {
            if(autoFillCustomerDetails.IsFeedAvailable(customerName.Text))
            {
                updateIsCustomerNameAvailableImg(true);
                var feed = autoFillCustomerDetails.FetchDetails(customerName.Text);
                addressL1.Text = feed.AddressLine1;
                addressL2.Text = feed.AddressLine2;
                addressL3.Text = feed.AddressLine3;
                customerMobile.Text = feed.MobileNumber;
                customerGSTIN.Text = feed.GSTIN;
            }
            else
            {
                updateIsCustomerNameAvailableImg(false);
                addressL1.Text = string.Empty;
                addressL2.Text = string.Empty;
                addressL3.Text = string.Empty;
                customerMobile.Text = string.Empty;
                customerGSTIN.Text = string.Empty;
            }
        }
    }
}
