﻿using Bytescout.Spreadsheet;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System;
using Randomize.Models;

namespace Excel2Json
{
    static class Program
    {
        static readonly Worksheet _excelStore;
        static Program()
        {
            _excelStore = loadExcelFile();
        }
        static Worksheet loadExcelFile()
        {
            var excelPath = string.Format("{0}Malaimagal.xlsx", ConfigurationManager.AppSettings["excelPath"]);
            var excel = new Spreadsheet();
            excel.LoadFromFile(excelPath);
            return excel.Workbook.Worksheets.ByName("Sheet1");
        }
        static List<ItemsModel> excelList()
        {
            var list = new List<ItemsModel>();
            var maxRows = _excelStore.UsedRangeRowMax;
            Func<string[], bool> validate = (input) =>
             {
                 bool isValid = true;
                 foreach (var item in input)
                 {
                     if (string.IsNullOrEmpty(item))
                         isValid = false;
                 }
                 return isValid;
             };
            for (int xIndex = 1; xIndex <= maxRows; xIndex++)
            {
                var itemName = _excelStore.Cell(xIndex, 0).ValueAsString;
                var salePrice = _excelStore.Cell(xIndex, 5).ValueAsString;
                var gst = _excelStore.Cell(xIndex, 9).ValueAsString;
                var hsn = _excelStore.Cell(xIndex, 8).ValueAsString;
                var per = _excelStore.Cell(xIndex,4).ValueAsString;

                if(validate(new [] { itemName, salePrice, gst, hsn }))
                {
                    Console.WriteLine($"Importing {xIndex}'th item ..");
                    list.Add( new ItemsModel
                    {
                        ItemName = itemName,
                        SalePrice = Convert.ToDecimal(salePrice),
                        GST = Convert.ToDecimal(gst),
                        Hsn = hsn,
                        Per = per
                    });
                }
            }
            return list;
        }
        static void generateJson()
        {
            var json = JsonConvert.SerializeObject(excelList());
            var jsonPath = string.Format("{0}AksGst.json", ConfigurationManager.AppSettings["jsonPath"]);
            using (var stream = new StreamWriter(jsonPath))
            {
                stream.Write(json);
            }
        }
        static void Main(string[] args)
        {
            generateJson();
        }
    }
}
