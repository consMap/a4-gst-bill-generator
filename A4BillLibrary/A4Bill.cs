﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Xml;
using A4BillLibrary.BillModel;
using A4BillLibrary.BillModel.HeaderModel;
using A4BillLibrary.BillModel.ItemsModel;
using System.Globalization;
using A4BillRender;
using DirectPrint = PrintReceipt.PrintReceipt;

namespace A4BillLibrary
{
    public static class A4Bill
    {
        public static string invoiceNumber { get; private set; }
        public static string dateStamp { get; private set; }

        private static string currentSNo;
        private static string appConfigPath;
        private static string outputPath;
        private static decimal amountTotal;
        private static decimal cgstTotal;
        private static decimal sgstTotal;

        static A4Bill()
        {
            try
            {
                appConfigPath = ConfigurationManager.AppSettings["appConfigPath"];
                outputPath = ConfigurationManager.AppSettings["outputPath"];
                invoiceNumber = manageInvoiceNumber();
                dateStamp = DateTime.Now.ToString("dd/MM/yyyy h:mm tt");
                currentSNo = "0";
            }
            catch (Exception)
            {
                throw new ApplicationException("lol");
            }

        }
        public static void generateBill(Bill input)
        {
            var billSchema = new XmlDocument();
            var imageBill = new RenderImage.Bill
            {
                header = new RenderImage.Bill.Header { DeliveredToDetails = new RenderImage.Bill.Header.DeliveredToDetail(), BuyerDetails = new RenderImage.Bill.Header.BuyerDetail() },
                footer = new RenderImage.Bill.Footer { TotalTaxInWords = new RenderImage.Bill.Footer.TwoLine(), AmountInWords = new RenderImage.Bill.Footer.TwoLine() },
                Items = new List<RenderImage.Bill.Item>()
            };

            var billName = string.Format("{1} - {2}_{0}", input.BillHeader.billDetails.paymentMode, input.BillHeader.billDetails.invoiceNo, input.BillHeader.clientDetails.customerName);
            billSchema.Load(string.Format("{0}SchemaV3.xml", appConfigPath));

            billSchema = fillHeader(billSchema, input.BillHeader, imageBill.header);
            billSchema = fillItemTable(billSchema, mergedItemsList(input.BilledItems), imageBill);

            var roundOff = decimal.Round(amountTotal - decimal.Floor(amountTotal), 2);
            var amountPayable = amountTotal - roundOff;
            var totalTax = sgstTotal + cgstTotal;

            billSchema.SelectSingleNode("//div[@id='totalAmountValue']").InnerText = indentCurrency(amountTotal);
            billSchema.SelectSingleNode("//div[@id='roundOffValue']").InnerText = string.Format("(-){0}", roundOff);
            imageBill.footer.RoundOff = roundOff.ToString();
            billSchema.SelectSingleNode("//div[@id='sgstTotal']/span").InnerText = sgstTotal.ToString();
            imageBill.footer.TotalSGST = sgstTotal.ToString();
            billSchema.SelectSingleNode("//div[@id='cgstTotal']/span").InnerText = cgstTotal.ToString();
            imageBill.footer.TotalCGST = cgstTotal.ToString();
            billSchema.SelectSingleNode("//div[@id='taxTotal']/span").InnerText = Convert.ToString(totalTax);
            imageBill.footer.TotalTax = totalTax.ToString();
            billSchema.SelectSingleNode("//div[@id='taxTotalWords']/span").InnerText = currencyInWords(totalTax);
            imageBill.footer.TotalTaxInWords.Line1 = splitLines(currencyInWords(totalTax),42)[0];
            imageBill.footer.TotalTaxInWords.Line2 = splitLines(currencyInWords(totalTax),42)[1];
            billSchema.SelectSingleNode("//span[@id='amountNumbersValue']").InnerText = indentCurrency(amountPayable);
            imageBill.footer.AmountPayable = indentCurrency(amountPayable);
            billSchema.SelectSingleNode("//span[@id='amountWordsValue']").InnerText = currencyInWords(amountPayable);
            imageBill.footer.AmountInWords.Line1 = splitLines(currencyInWords(amountPayable),49)[0];
            imageBill.footer.AmountInWords.Line2 = splitLines(currencyInWords(amountPayable),49)[1];
            billSchema.SelectSingleNode("//title[@id='billTitle']").InnerText = billName;

            new RenderImage($"{appConfigPath}A4_Bill_Template.png").Render($"{outputPath}{billName}.png", imageBill);
            billSchema.Save(string.Format("{0}{1}.html", outputPath, billName));
            new DirectPrint($"{outputPath}{billName}.png").Print();
        }

        private static string[] splitLines(string input, int count)
        {
            string line1 = string.Empty;
            string line2 = string.Empty;
            var skippedLine2 = true;
            if (input.Count()>count)
            {
                var words = input.Split(' ');
                foreach (var word in words)
                {
                    if(word.Length + line1.Length < count && skippedLine2)
                    {
                        line1 += word+" ";
                    }
                    else
                    {
                        skippedLine2 = false;
                        line2 += word+" ";
                    }
                }
                return new []{ line1,line2};
            }
            return new[] { input, null };
        }

        private static string currencyInWords(decimal input)
        {
            bool isNegative = false;
            string output;
            if (input < 0M)
            {
                input = decimal.Negate(input);
                isNegative = true;
            }
            var rupees = Convert.ToInt32(decimal.Floor(input));
            var paise = Convert.ToInt32((input - rupees) * 100);
            var rupeeIteration = rupees.ToString().Count();
            var paiseIteration = paise.ToString().Count();
            var rupeesInWords = string.Empty;
            var paiseInWords = string.Empty;
            for (int count = 0; count < rupeeIteration; count++)
            {
                if (rupees != 0)
                {
                    string digitWord;
                    if (count == 0)
                    {
                        var digit = rupees % 100;
                        var wordDiction = new Dictionary<int, string>{{ 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" },{ 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" }, {10,"Ten" },
                            {11,"Eleven" }, {12,"Twelve" }, { 13, "Thirteen"}, {14,"Fourteen" }, {15,"Fifteen" }, {16,"Sixteen" }, { 17,"Seventeen"}, { 18,"Eighteen"}, {19,"Nineteen" } };
                        var isBelow20 = wordDiction.TryGetValue(digit, out digitWord);
                        if (!isBelow20)
                        {
                            var wordDiction2 = new Dictionary<int, string> { { 2, "Twenty" }, { 3, "Thirty" }, { 4, "Fourty" }, { 5, "Fifty" }, { 6, "Sixty" }, { 7, "Seventy" }, { 8, "Eighty" }, { 9, "Ninety" } };
                            var subDigitLast = digit % 10;
                            var subDigitFirst = digit / 10;
                            string last, first;
                            wordDiction.TryGetValue(subDigitLast, out last);
                            wordDiction2.TryGetValue(subDigitFirst, out first);
                            digitWord = string.Format("{0} {1}", first, last);
                        }
                        if (!string.IsNullOrEmpty(digitWord))
                            rupeesInWords = string.Format("{1}{0}", rupeesInWords, digitWord);
                        rupees /= 100;
                    }
                    if (count == 1)
                    {
                        var digit = rupees % 10;
                        var wordDiction = new Dictionary<int, string> { { 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" }, { 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" } };
                        var isHundreds = wordDiction.TryGetValue(digit, out digitWord);
                        if (!string.IsNullOrEmpty(digitWord) && string.IsNullOrWhiteSpace(rupeesInWords))
                            rupeesInWords = string.Format("{1} hundred{0}", rupeesInWords, digitWord);
                        else if (!string.IsNullOrEmpty(digitWord))
                            rupeesInWords = string.Format("{1} hundred And {0}", rupeesInWords, digitWord);
                        rupees /= 10;
                    }
                    if (count == 2)
                    {
                        var digit = rupees % 100;
                        var wordDiction = new Dictionary<int, string>{{ 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" },{ 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" }, {10,"Ten" },
                            {11,"Eleven" }, {12,"Twelve" }, { 13, "Thirteen"}, {14,"Fourteen" }, {15,"Fifteen" }, {16,"Sixteen" }, { 17,"Seventeen"}, { 18,"Eighteen"}, {19,"Nineteen" } };
                        var isBelow20 = wordDiction.TryGetValue(digit, out digitWord);
                        if (!isBelow20)
                        {
                            var wordDiction2 = new Dictionary<int, string> { { 2, "Twenty" }, { 3, "Thirty" }, { 4, "Fourty" }, { 5, "Fifty" }, { 6, "Sixty" }, { 7, "Seventy" }, { 8, "Eighty" }, { 9, "Ninety" } };
                            var subDigitLast = digit % 10;
                            var subDigitFirst = digit / 10;
                            string last, first;
                            wordDiction.TryGetValue(subDigitLast, out last);
                            wordDiction2.TryGetValue(subDigitFirst, out first);
                            digitWord = string.Format("{0} {1}", first, last);
                        }
                        if (!string.IsNullOrEmpty(digitWord))
                            rupeesInWords = string.Format("{1} Thousand {0}", rupeesInWords, digitWord);
                        rupees /= 100;
                    }
                    if (count == 3)
                    {
                        var digit = rupees % 100;
                        var wordDiction = new Dictionary<int, string>{{ 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" },{ 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" }, {10,"Ten" },
                            {11,"Eleven" }, {12,"Twelve" }, { 13, "Thirteen"}, {14,"Fourteen" }, {15,"Fifteen" }, {16,"Sixteen" }, { 17,"Seventeen"}, { 18,"Eighteen"}, {19,"Nineteen" } };
                        var isBelow20 = wordDiction.TryGetValue(digit, out digitWord);
                        if (!isBelow20)
                        {
                            var wordDiction2 = new Dictionary<int, string> { { 2, "Twenty" }, { 3, "Thirty" }, { 4, "Fourty" }, { 5, "Fifty" }, { 6, "Sixty" }, { 7, "Seventy" }, { 8, "Eighty" }, { 9, "Ninety" } };
                            var subDigitLast = digit % 10;
                            var subDigitFirst = digit / 10;
                            string last, first;
                            wordDiction.TryGetValue(subDigitLast, out last);
                            wordDiction2.TryGetValue(subDigitFirst, out first);
                            digitWord = string.Format("{0} {1}", first, last);
                        }
                        if (!string.IsNullOrEmpty(digitWord))
                            rupeesInWords = string.Format("{1} Lakh {0}", rupeesInWords, digitWord);
                        rupees /= 100;
                    }
                    if (count == 4)
                    {
                        var digit = rupees % 100;
                        var wordDiction = new Dictionary<int, string>{{ 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" },{ 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" }, {10,"Ten" },
                            {11,"Eleven" }, {12,"Twelve" }, { 13, "Thirteen"}, {14,"Fourteen" }, {15,"Fifteen" }, {16,"Sixteen" }, { 17,"Seventeen"}, { 18,"Eighteen"}, {19,"Nineteen" } };
                        var isBelow20 = wordDiction.TryGetValue(digit, out digitWord);
                        if (!isBelow20)
                        {
                            var wordDiction2 = new Dictionary<int, string> { { 2, "Twenty" }, { 3, "Thirty" }, { 4, "Fourty" }, { 5, "Fifty" }, { 6, "Sixty" }, { 7, "Seventy" }, { 8, "Eighty" }, { 9, "Ninety" } };
                            var subDigitLast = digit % 10;
                            var subDigitFirst = digit / 10;
                            string last, first;
                            wordDiction.TryGetValue(subDigitLast, out last);
                            wordDiction2.TryGetValue(subDigitFirst, out first);
                            digitWord = string.Format("{0} {1}", first, last);
                        }
                        if (!string.IsNullOrEmpty(digitWord))
                            rupeesInWords = string.Format("{1} Crore {0}", rupeesInWords, digitWord);
                        rupees /= 100;
                    }
                }
                else if (count == 0)
                {
                    rupeesInWords = "Zero";
                }
            }
            if (paise != 0)
            {
                string digitWord;
                var digit = paise % 100;
                var wordDiction = new Dictionary<int, string>{{ 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" },{ 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" }, {10,"Ten" },
                            {11,"Eleven" }, {12,"Twelve" }, { 13, "Thirteen"}, {14,"Fourteen" }, {15,"Fifteen" }, {16,"Sixteen" }, { 17,"Seventeen"}, { 18,"Eighteen"}, {19,"Nineteen" } };
                var isBelow20 = wordDiction.TryGetValue(digit, out digitWord);
                if (!isBelow20)
                {
                    var wordDiction2 = new Dictionary<int, string> { { 2, "Twenty" }, { 3, "Thirty" }, { 4, "Fourty" }, { 5, "Fifty" }, { 6, "Sixty" }, { 7, "Seventy" }, { 8, "Eighty" }, { 9, "Ninety" } };
                    var subDigitLast = digit % 10;
                    var subDigitFirst = digit / 10;
                    string last, first;
                    wordDiction.TryGetValue(subDigitLast, out last);
                    wordDiction2.TryGetValue(subDigitFirst, out first);
                    digitWord = string.Format("{0} {1}", first, last);
                }
                if (!string.IsNullOrEmpty(digitWord))
                    paiseInWords = string.Format("{1} {0}", paiseInWords, digitWord);
            }
            output = string.IsNullOrEmpty(rupeesInWords) ? "Invalid Amount" : string.IsNullOrEmpty(paiseInWords) ? string.Format("{0} Rupees Only", rupeesInWords) : string.Format("{0} Rupees {1} Paise Only", rupeesInWords, paiseInWords);
            if (isNegative)
            {
                return string.Format("Minus {0}", output);
            }
            else
            {
                return output;
            }
        }
        private static string indentCurrency(decimal input)
        {
            var output = string.Empty;
            var formatter = (NumberFormatInfo)new CultureInfo("en-IN").NumberFormat.Clone();
            formatter.CurrencySymbol = "Rs";
            output = string.Format(formatter, "{0:C}", input);
            return output;
        }
        private static XmlDocument fillHeader(XmlDocument input, Header source, RenderImage.Bill.Header imgHeader)
        {
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition2']/div[@id='clientDetail']/span[@id='clientName']").InnerText = string.Format("Mr/Ms {0}", source.clientDetails.customerName);
            imgHeader.BuyerDetails.Line1 = string.Format("Mr/Ms {0}", source.clientDetails.customerName);
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition2']/div[@id='clientDetail']/span[@id='clientAddressR1']").InnerText = source.clientDetails.addressL1;
            imgHeader.BuyerDetails.Line2 = source.clientDetails.addressL1;
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition2']/div[@id='clientDetail']/span[@id='clientAddressR2']").InnerText = source.clientDetails.addressL2;
            imgHeader.BuyerDetails.Line3 = source.clientDetails.addressL2;
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition2']/div[@id='clientDetail']/span[@id='clientAddressR3']").InnerText = source.clientDetails.addressL3;
            imgHeader.BuyerDetails.Line4 = source.clientDetails.addressL3;
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition2']/div[@id='clientDetail']/span[@id='clientPhone']").InnerText = string.Format("Phone : {0}", source.clientDetails.customerMobile);
            imgHeader.BuyerDetails.Line5 = string.Format("Phone : {0}", source.clientDetails.customerMobile);
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition2']/div[@id='clientDetail']/span[@id='clientGstin']").InnerText = source.clientDetails.customerGSTIN;
            imgHeader.GSTIN = source.clientDetails.customerGSTIN;

            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition2']/div[@id='reference']/span[@id='details']").InnerText = source.billDetails.billReferences;
            imgHeader.BuyerDetails.Line6 = string.Format("Bill Reference : {0}", source.billDetails.billReferences);
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition3']/div[@id='invoice']/span[@id='number']").InnerText = source.billDetails.invoiceNo;
            imgHeader.Invoice = source.billDetails.invoiceNo;
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition3']/div[@id='date']//span[@id='timestamp']").InnerText = source.billDetails.billDate;
            imgHeader.Date = source.billDetails.billDate;
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition3']/div[@id='payment']//span[@id='mode']").InnerText = source.billDetails.paymentMode;
            imgHeader.PaymentMethod = source.billDetails.paymentMode;
            input.SelectSingleNode("//div[@id='HeadBlock']/div[@id='partition3']/div[@id='deliveryAddress']/span[@id='to']").InnerText = source.billDetails.deliveredTo;
            imgHeader.DeliveredToDetails.Line1 = source.billDetails.deliveredTo;

            return input;
        }
        private static XmlDocument fillItemTable(XmlDocument input, List<Items> source, RenderImage.Bill imgBill)
        {
            var rowIndex = 1;
            var billingList = new List<RenderImage.Bill.Item>();
            foreach (Items item in source)
            {
                var itm = new RenderImage.Bill.Item();
                input.SelectSingleNode(string.Format("//span[@id='c1r{0}']", rowIndex)).InnerText = item.manualFields.Sno;
                itm.Sno = item.manualFields.Sno;
                input.SelectSingleNode(string.Format("//span[@id='c2r{0}']", rowIndex)).InnerText = item.manualFields.ItemName;
                itm.ItemName = item.manualFields.ItemName;
                input.SelectSingleNode(string.Format("//span[@id='c3r{0}']", rowIndex)).InnerText = item.manualFields.Hsn;
                itm.Hsn = item.manualFields.Hsn;
                input.SelectSingleNode(string.Format("//span[@id='c4r{0}']", rowIndex)).InnerText = item.automaticFields.Rate;
                itm.Rate = item.automaticFields.Rate;
                input.SelectSingleNode(string.Format("//span[@id='c6r{0}']", rowIndex)).InnerText = item.manualFields.Qty;
                itm.Qty = item.manualFields.Qty;
                input.SelectSingleNode(string.Format("//span[@id='c5r{0}']", rowIndex)).InnerText = item.manualFields.Per;
                itm.Unit = item.manualFields.Per;
                input.SelectSingleNode(string.Format("//span[@id='c7r{0}']", rowIndex)).InnerText = item.manualFields.Discount;
                input.SelectSingleNode(string.Format("//span[@id='c8r{0}']", rowIndex)).InnerText = item.automaticFields.grossAmount;
                itm.GrossAmount = item.automaticFields.grossAmount;
                input.SelectSingleNode(string.Format("//span[@id='c9r{0}']", rowIndex)).InnerText = item.manualFields.GST;
                itm.GstPercentage = item.manualFields.GST;
                input.SelectSingleNode(string.Format("//span[@id='c10r{0}']", rowIndex)).InnerText = item.automaticFields.cgstAmount;
                itm.CGST = item.automaticFields.cgstAmount;
                input.SelectSingleNode(string.Format("//span[@id='c11r{0}']", rowIndex)).InnerText = item.automaticFields.sgstAmount;
                itm.SGST = item.automaticFields.sgstAmount;
                input.SelectSingleNode(string.Format("//span[@id='c12r{0}']", rowIndex)).InnerText = item.automaticFields.nettPrice;
                itm.NetAmount = item.automaticFields.nettPrice;
                billingList.Add(itm);
                rowIndex++;
                cgstTotal += Convert.ToDecimal(item.automaticFields.cgstAmount);
                sgstTotal += Convert.ToDecimal(item.automaticFields.sgstAmount);
                amountTotal += Convert.ToDecimal(item.automaticFields.nettPrice);
            }
            imgBill.Items = billingList;
            return input;
        }
        private static List<Items> mergedItemsList(List<BillingItemsManual> input)
        {
            return (from manualItem in input
                    let cgstRate = Convert.ToDecimal(manualItem.GST) / 200
                    let sgstRate = Convert.ToDecimal(manualItem.GST) / 200
                    let gstRate = cgstRate + sgstRate
                    let qty = Convert.ToDecimal(manualItem.Qty)
                    let salePrice = Convert.ToDecimal(manualItem.SalePrice)
                    let nettAmount = salePrice * qty
                    let rate = decimal.Round(salePrice / (1 + gstRate), 2)
                    let discountPercent = Convert.ToDecimal(manualItem.Discount) / 100
                    let discountAmount = rate * discountPercent
                    let grossAmount = decimal.Round((rate - discountAmount) * qty, 2)
                    let cgstAmount = decimal.Round(rate * qty * cgstRate, 2)
                    let sgstAmount = decimal.Round(rate * qty * sgstRate, 2)
                    select new Items
                    {
                        manualFields = manualItem,
                        automaticFields = new BillingItemsAutoGen
                        {
                            grossAmount = grossAmount.ToString(),
                            cgstAmount = cgstAmount.ToString(),
                            sgstAmount = sgstAmount.ToString(),
                            Rate = rate.ToString(),
                            nettPrice = nettAmount.ToString()
                        }
                    }).ToList();
        }
        public static string sNo()
        {
            currentSNo = (Convert.ToInt32(currentSNo) + 1).ToString();
            return currentSNo;
        }
        public static string manageInvoiceNumber()
        {
            var configXml = new XmlDocument();
            try
            {
                configXml.Load(string.Format("{0}a4Config.xml", appConfigPath));
                var invoiceNumber = configXml.SelectSingleNode("//invoice").InnerText;
                invoiceNumber = (Convert.ToInt32(invoiceNumber) + 1).ToString();
                configXml.SelectSingleNode("//invoice").InnerText = invoiceNumber;
                configXml.Save(string.Format("{0}a4Config.xml", appConfigPath));
                return invoiceNumber;
            }
            catch (Exception)
            {
                throw new ApplicationException("Could Not load Invoice Number");
            }
        }
        public static void validateInvoiceNumber(string currentInvoiceNumber)
        {
            if (currentInvoiceNumber != invoiceNumber)
            {
                var configXml = new XmlDocument();
                try
                {
                    configXml.Load(string.Format("{0}a4Config.xml", appConfigPath));
                    configXml.SelectSingleNode("//invoice").InnerText = currentInvoiceNumber;
                    configXml.Save(string.Format("{0}a4Config.xml", appConfigPath));
                }
                catch (Exception)
                {
                    throw new ApplicationException("Problem saving the new invoice");
                }
            }
        }
    }
}
