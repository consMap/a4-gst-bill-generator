﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using A4BillLibrary.BillModel.HeaderModel;
using A4BillLibrary.BillModel.ItemsModel;

namespace A4BillLibrary.BillModel
{
    public class Bill
    {
        public Header BillHeader { get; set; }
        public List<BillingItemsManual> BilledItems { get; set; }
    }
}
