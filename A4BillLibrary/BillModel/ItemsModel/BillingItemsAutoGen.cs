﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4BillLibrary.BillModel.ItemsModel
{
    internal class BillingItemsAutoGen
    {
        internal string grossAmount { get; set; }
        internal string cgstAmount { get; set; }
        internal string sgstAmount { get; set; }
        internal string Rate { get; set; }
        internal string nettPrice { get; set; }
    }
}
