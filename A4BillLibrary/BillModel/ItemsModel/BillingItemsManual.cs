﻿namespace A4BillLibrary.BillModel.ItemsModel
{
    public class BillingItemsManual
    {
        public string Sno { get; set; }
        public string ItemName { get; set; }
        public string Hsn { get; set; }
        public string Qty { get; set; }
        public string SalePrice { get; set; }
        public string Discount { get; set; }
        public string Per { get; set; }
        public string GST { get; set; }
    }
}
