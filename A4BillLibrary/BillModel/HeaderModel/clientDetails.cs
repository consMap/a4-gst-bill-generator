﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4BillLibrary.BillModel.HeaderModel
{
    public class clientDetails
    {
        public string customerName { get; set; }
        public string addressL1 { get; set; }
        public string addressL2 { get; set; }
        public string addressL3 { get; set; }
        public string customerMobile { get; set; }
        public string customerGSTIN { get; set; }
    }
}
