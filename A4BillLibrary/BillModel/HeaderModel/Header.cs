﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4BillLibrary.BillModel.HeaderModel
{
    public class Header
    {
        public clientDetails clientDetails { get; set; }
        public billDetails billDetails { get; set; }
    }
}
