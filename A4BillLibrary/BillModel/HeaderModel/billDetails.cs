﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4BillLibrary.BillModel.HeaderModel
{
    public class billDetails
    {
        public string paymentMode { get; set; }
        public string billReferences { get; set; }
        public string deliveredTo { get; set; }
        public string invoiceNo { get; set; }
        public string billDate { get; set; }
    }
}
