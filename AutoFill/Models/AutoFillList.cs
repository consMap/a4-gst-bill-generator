﻿using System.Collections.Generic;

namespace AutoFill.Models
{
    public class CustomerDetails
    {
        public string GSTIN { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string MobileNumber { get; set; }
    }
}
