﻿using System.Collections.Generic;
using System.Configuration;
using Newtonsoft.Json;
using System.IO;
using AutoFill.Models;
using System;

namespace AutoFill
{
    public class AutoFill
    {
        public string Feed { get; private set; }
        private Dictionary<string, CustomerDetails> _customerDetailsList { get; set; }
        private string _path;

        public AutoFill()
        {
            _path = string.Format("{0}CustomerDetails.json", ConfigurationManager.AppSettings["jsonPath"]);
            _customerDetailsList = new Dictionary<string, CustomerDetails>();
            loadList();
        }

        public bool IsFeedAvailable(string customerName)
        {
            return _customerDetailsList.ContainsKey(customerName);
        }

        public CustomerDetails FetchDetails(string customerName)
        {
            return _customerDetailsList[customerName];
        }

        public void ModifyRecords(string customerName, CustomerDetails customerDetails)
        {
            Func<bool> isRecordsModified = () => 
            {
                var newRecords = customerDetails;
                var oldRecords = FetchDetails(customerName);
                if(newRecords.GSTIN != oldRecords.GSTIN)
                {
                    return true;
                }
                if (newRecords.MobileNumber != oldRecords.MobileNumber)
                {
                    return true;
                }
                if (newRecords.AddressLine1 != oldRecords.AddressLine1)
                {
                    return true;
                }
                if (newRecords.AddressLine2 != oldRecords.AddressLine2)
                {
                    return true;
                }
                if (newRecords.AddressLine3 != oldRecords.AddressLine3)
                {
                    return true;
                }
                return false;
            };
            if(!IsFeedAvailable(customerName))
            {
                _customerDetailsList.Add(customerName, customerDetails);
                saveList();
            }
            else if(IsFeedAvailable(customerName) && isRecordsModified())
            {
                _customerDetailsList.Remove(customerName);
                _customerDetailsList.Add(customerName, customerDetails);
                saveList();
            }
        }

        private void loadList()
        {
            if(File.Exists(_path))
            {
                var jsonFile = File.ReadAllText(_path);
                _customerDetailsList = JsonConvert.DeserializeObject<Dictionary<string, CustomerDetails>>(jsonFile);
            }
        }

        private void saveList()
        {
            var jsonFile = JsonConvert.SerializeObject(_customerDetailsList);
            File.WriteAllText(_path, jsonFile);
        }
    }
}
